# Kilavo Admin UI (WIP)

This repo is a simple Admin Interface that can manage multiple Kilavo nodes,
it connects to a node via libp2p, the first time you open this in your browser it
generates a libp2p identity, that you can then add as an administrator on your node via

```
go run cmd/kilavo.go config admins add <peerId>
```

after that, add the node in the Settings page and you are good to go

![UI Example](./screen.png "UI Example")

## Quick start
Run development server
```shell
yarn run serve
```
