module.exports = {
  presets: [
    ['@vue/app', { useBuiltIns: "entry" }]
  ],
  "env": {
    "development": {
      presets: [
        ['@vue/app', { useBuiltIns: "entry", jsx: { injectH: false } }]
      ]
    },
    "production": {
      presets: [
        ['@vue/app', { useBuiltIns: "entry", jsx: { injectH: false } }]
      ],
      "plugins": ["transform-remove-console"]
    }
  }
}