import Vue from 'vue';
import Vuex from 'vuex';
import layout from './layout';
import nodes from './nodes';
import libp2p from './libp2p';
import adminAPI from './adminAPI';
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    layout,
    libp2p,
    nodes,
    adminAPI,
  },
  plugins: [createPersistedState()],
});
