/* eslint-disable */
export default {
    namespaced: true,
    state: {
        identity: "",
        cid: "",
    },
    mutations: {
        saveNode(state, data) {
            console.log("SAVE",data)
            state.identity = data
        },
        saveCid(state, data) {
            state.cid = data
        },
    },
    actions: {
        save({ commit }, value) {
            commit('saveNode', value);
        },
        setCid({ commit }, value) {
            commit('saveCid', value);
        },
    },
};
