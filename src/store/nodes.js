/* eslint-disable */
export default {
    namespaced: true,
    state: {
        nodes: [],
        activeNode: "",
    },
    mutations: {
        addNode(state, data) {
            state.nodes.push(data)
        },
        clear(state, data) {
            state.nodes = []
        },
        removeNode(state, address) {
            state.nodes = state.nodes.filter(function(item) {
                return item.address !== address;
            });
            if (state.nodes === undefined || state.nodes === null) {
                state.nodes = []
            }
        },
    },
    actions: {
        addNode({ commit }, value) {
            value.status = "unkown"
            commit('addNode', value);
        },
        clear({ commit }, value) {
            commit('clear', value);
        },
        removeNode({ commit }, index) {
            commit('removeNode', index);
        },
    },
};
