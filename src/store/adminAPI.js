/* eslint-disable */
export default {
    namespaced: true,
    state: {
        nodes: [],
        federations: [],
    },
    mutations: {
        UpdateNodes(state, data) {
            state.nodes = window.p2p.nodes
        },
        MaybeAddFederation(state, data) {
            if ( state.federations.includes(data) === false) {
                state.federations.push(data)
            }
        },
    },
    actions: {
        startUpdates({ commit2 }, value) {
            var commit = commit2
            var updates = function (){
                if (window.p2p.nodes === undefined) {
                    return
                }
                for (let k in window.p2p.nodes) {
                    var node = window.p2p.nodes[k]
                    if (commit.state.nodes.includes(node)) {

                    } else {
                        commit('saveNode', value);
                        window.p2p.adminSub(node,"federations",function (data) {
                            console.log("GOT FEDERATION UPDATE")
                            var p = JSON.parse(data)
                            for (let i in federations ){
                                commit('MaybeAddFederation', p[i]);
                            }
                            }
                        )
                    }
                }
            }
            //window.setInterval(updates,500)
        },

    },
};
